<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'AppController@index')->name('index');
Route::get('process', function () {
    return redirect(route('index'));
});
Route::post('/process', 'AppController@process')->name('process');
Route::get('/download_csv', 'CsvController@generateCsv')->name('download_csv');
