<?php

namespace App\Http\Controllers;

use League\Csv\Writer;
use Illuminate\Http\Request;

class CsvController extends Controller {

    public function generateCsv(Request $request) {
        $input = $request->input('input');

        if ($input) {

            // CSV column is delimited by comma character (,)
            // Split the input
            $arr_input = str_split($input);

            // CSV Writer on-the-fly
            $csv = Writer::createFromFileObject(new \SplTempFileObject());
            $csv->insertOne($arr_input);

            $csv->output('download_' . md5(time()) . '.csv');
            die;
        }
    }

}
