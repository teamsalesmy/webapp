<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller {

    public function index() {
        return view('welcome');
    }

    // Dependency Injection on Method
    public function process(Request $request) {
        // Get HTTP Request
        $input = $request->input('input');

        // Transform to Upper Case
        $upper = strtoupper($input);

        // Transform to Alternate Upper Case
        $str_arr = str_split($input); // Split the input by each character into array
        $alt_upper = []; // New array to hold new values
        for ($i = 0; $i < count($str_arr); $i++) { // Loop through splitted input and get alternated character by odd/even index
            if ($i % 2 == 0) {
                $alt_upper[] = $str_arr[$i];
            } else {
                $alt_upper[] = strtoupper($str_arr[$i]);
            }
        }

        $alt_upper = implode('', $alt_upper);

        return view('welcome')->with(compact('upper', 'alt_upper'));
    }

}
